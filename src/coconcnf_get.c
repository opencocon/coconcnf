/*
 * readcnf_get.c
 * 
 * Copyright 2024 SHIMADA Hirofumi <shimada at opencocon.org>
 * 
 */

#include <libconfig.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "md5.h"
#include "coconcnf.h"
#include "config.h"

#define MODE_BOOTFLAGS  1
#define MODE_PROFILES   2
#define MODE_CONNFLAGS  3


void usage(char *name)
{
	fprintf(stderr, "Usage: %s -m merged_file [option]\n", name);
	fprintf(stderr, "   -b : fetch boot flags to stdout\n");
	fprintf(stderr, "   -p : fetch profile list to stdout\n");
	fprintf(stderr, "   -s [profile_id] : fetch setup and auto-connection flags to stdout\n");
}


/* output config to stdout */
void output_option_on_config(config_setting_t *cur, const char *name, int type)
{
	int value_i;
	const char *value_s;

	switch(type) {
		/* boolean */
		case OPTION_BOOL:
			value_i = config_setting_get_bool(cur);
			if (value_i != 0) {
				printf("%s=%d\n", name, value_i);
			}
			break;

		/* string */
		case OPTION_STR:
			value_s = config_setting_get_string(cur);
			if (value_s != NULL) {
				printf("%s=%s\n", name, value_s);
			}
			break;
					
		/* integer */
		case OPTION_INT:
			value_i = config_setting_get_int(cur);
			if (value_i != 0) {
				printf("%s=%d\n", name, value_i);
			}
			break;
		/* file */
		case OPTION_FILE:
			/* TODO */
			break;
	}
}


int get_option_type(const char *name)
{
	int type = 0;

	/* TODO : このとき、typeがわからないと値を取り出せないが、どうするか？ */
	/* -> config_setting_lookup_int() などを使うか、allow_optionでループをまわしたほうがよい */
	for (int i=0; i < allow_option_size; i++) {
		if (allow_option[i].cfgtype == OPTION_AUTOCONN && strcmp(allow_option[i].name, name) == 0 ) {
			type = allow_option[i].type;
		}
	}

	return type;
}


/* Main Routine */
int main(int argc, char **argv)
{
	int arg;
	char *profile_id = NULL;
	char *merged_file = NULL;
	const char *cur_parm_name;
	config_t repo;
	config_setting_t *repo_root, *repo_auto, *repo_boot, *cur, *cur_name, *cur_parm;
	int mode = 0;
	int mode_error = 0;
	int cur_parm_type;
	const char *value_s;


	/* parse options */
	while((arg = getopt(argc, argv, "bps:m:?")) != -1)
	{
		switch(arg)
		{
			case 'b':	/* fetch boot flags to stdout */
				if (mode != 0)
					mode_error = 1;
				else
					mode = MODE_BOOTFLAGS;
				break;
			case 'p':   /* fetch profile list to stdout */
				if (mode != 0)
					mode_error = 1;
				else
					mode = MODE_PROFILES;
				break;
			case 's':	/* fetch setup and auto-connection flags to stdout(arg: profile id) */
				if (mode != 0)
					mode_error = 1;
				else{
					mode = MODE_CONNFLAGS;
					profile_id = optarg;
				}
				break;
			case 'm':	/* merged config file (aka. repository) */
				merged_file = optarg;
				break;
			case '?':
				usage(argv[0]);
				return 1;
		}
	}

	/* check parameter is valid */
	if (mode_error == 1) {
		fprintf(stderr, "Error: cannot select multiple get flag.\n\n");
		usage(argv[0]);
		return 1;
	}

	if (mode == 0) {
		fprintf(stderr, "Error: please specify a get flag.\n\n");
		usage(argv[0]);
		return 1;
	}

	if (merged_file == NULL || access(merged_file, R_OK) != 0) {
		fprintf(stderr, "Error: merged file not found, or not readable.\n");
		return 1;
	}else{
		fprintf(stderr, "merged_file: %s\n", merged_file);
	}

	/* init repository */
	config_init(&repo);

	/* open repository file */
	if (config_read_file(&repo, merged_file) != CONFIG_TRUE) {
		fprintf(stderr, "Error: failed to open merged file.\n");
		config_destroy(&repo);
		return 1;
	}

	/* pickup root */
	repo_root = config_root_setting(&repo);

	/* ここから下では、たとえばsectionが無い場合であっても、何も設定されていないものと扱い、正常に終了する。 */
	if (mode == MODE_BOOTFLAGS) {
		/* fetch boot flags */
		repo_boot = config_setting_get_member(repo_root, "boot");
		if (repo_boot != NULL) {
			for (int i=0; i < allow_option_size; i++) {

				if (allow_option[i].cfgtype != OPTION_SYSCONFIG) {
					continue;
				}
				
				/* try to get option from repository */
				cur = config_setting_lookup(repo_boot, allow_option[i].name);
				if (cur == NULL) {
					continue;
				}

				output_option_on_config(cur, allow_option[i].name, allow_option[i].type);
			}
		}
	}else if(mode == MODE_PROFILES){
		/* get profile list */
		repo_auto = config_setting_get_member(repo_root, "auto");

		if (repo_auto != NULL) {
			for ( unsigned int i = 0; ; i++ ) {
				cur = config_setting_get_elem(repo_auto, i);
				if (cur == NULL)
					break;

				/* pickup profile name */
				cur_name = config_setting_lookup(cur, "NAME");
				if (cur_name == NULL)
					break;
				value_s = config_setting_get_string(cur_name);
				if (strlen(value_s) <= 0)
					value_s = "<Default Setting>";
				printf("%d\t%s\n", i, value_s);
			}
		}
	}else if(mode == MODE_CONNFLAGS) {
		/* pickup auto(connect) section */
		repo_auto = config_setting_get_member(repo_root, "auto");

		if (repo_auto == NULL) {
			/* auto section was not found (not warning) */
			fprintf(stderr, "INFO : auto section is not found.\n");
			config_destroy(&repo);
			return 0;
		}

		/* and pickup by profile_id */
		cur = config_setting_get_elem(repo_auto, atoi(profile_id));
		if (cur == NULL) {
			/* profile number is unknown */
			fprintf(stderr, "WARNING : auto section number is not found.\n");
			config_destroy(&repo);
			return 1;
		}

		int cur_len = config_setting_length(cur);
		for ( unsigned int i = 0; i < (unsigned int)cur_len; i++ ) {
			cur_parm = config_setting_get_elem(cur, i);
			if (cur_parm == NULL)
				break;

			cur_parm_name = config_setting_name(cur_parm);

			/* if parameter is "NAME", skip this. */
			if (strcmp(cur_parm_name, "NAME") == 0)
				continue;

			cur_parm_type = get_option_type(cur_parm_name);
			if (cur_parm_type == 0)
				continue;

			output_option_on_config(cur_parm, cur_parm_name, cur_parm_type);
		}
	}

	config_destroy(&repo);
	return 0;
}
