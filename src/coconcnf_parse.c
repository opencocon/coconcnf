/*
 * readcnf_parse.c
 * 
 * Copyright 2024 SHIMADA Hirofumi <shimada at opencocon.org>
 * 
 */

#include <libconfig.h>
#include <ctype.h>
#include <errno.h>
#include <ini.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "md5.h"
#include "coconcnf.h"
#include "config.h"


/* struct flags of cocon.cnf */
typedef struct
{
	/* common flags */
	int boot_only_config;
	config_t *repo;
	config_setting_t *repo_auto, *repo_boot;
} configuration;


/* convert alphabet to upper */
/* return value is heap memory aloced by malloc() */
char *string_upper(const char *str)
{
	char *c = strdup(str);
	int i;
	
	for(i=0; i < strlen(str); i++)
	{
		c[i] = toupper((unsigned char) c[i]);
	}
	return c;
}


config_setting_t *find_or_make_subsection(config_setting_t *section, const char *subsection_name, int type) {
	config_setting_t *subsection = config_setting_get_member(section, subsection_name);
	if (subsection == NULL)
		subsection = config_setting_add(section, subsection_name, type);

	/* TODO : if fail to create, output error */
	if (subsection == NULL)
		printf("ERROR : failed on find_or_make_subsection (name: %s)\n", subsection_name);

	return subsection;
}


void add_autoconn_profile_name(config_setting_t *section, const char *profile_name) {
	config_setting_t *nameconf = config_setting_get_member(section, "NAME");
	if (nameconf == NULL) {
		nameconf = config_setting_add(section, "NAME", CONFIG_TYPE_STRING);
		config_setting_set_string(nameconf, profile_name);
	}
}


/* parse handler for config file */
static int parser_handler(void* user, const char* section, const char* name,
                   const char* value)
{
	/* fail if name is unknown */
	if (name == NULL || strlen(name) <= 0)
		return 0;

	int found = 0, value_i;
	char *name_upper = string_upper(name);
	configuration *config = user;
	config_setting_t *addconf, *auto_subsection;
	/* magic code "MD5-" 4byte + string of md5 hash size + end code "\0" 1 byte */
	char subsection_name[37];
	
	/* subsection name : default name is "DEFAULT" */
	if (strlen(section) > 0) {
		uint8_t md5_result[16];
		md5String(section, md5_result);
		subsection_name[0] = 'M';
		subsection_name[1] = 'D';
		subsection_name[2] = '5';
		subsection_name[3] = '_';
		for(int i=0; i < 16; i++){
			sprintf(& subsection_name[4 + (i * 2)], "%02x", md5_result[i]);
		}
		subsection_name[36] = '\0';
	}else{
		strcpy(subsection_name, "DEFAULT");
	}
	// TODO : 16byte列でかえってくるので、32byteのstring(hex)に変換しなければならない。
	printf("section md5 : %s\n", subsection_name);

	/* scan values */
	for (int i=0; i < allow_option_size && found == 0; i++) {
		struct allowopt *cur = &allow_option[i];
		// printf("search: %s (type=%d)\n", cur->name, cur->type);

		/* if match option... */
		if(strcmp(cur->name, name_upper) == 0 && 
			( cur->bootprm == ALWAYS || (cur->bootprm == ONLY_BOOTING && config->boot_only_config == 1) )) {
			/* match */
			printf("Match ---- sec:%s name:%s value:%s\n", section, name_upper, value);
			found = 1;
			
			switch(cur->type) {
				/* option is boolean */
				case OPTION_BOOL:
					/* convert to integer */
					value_i = atoi(value);
			
					/* only 1 is valid (TODO : another string) */
					if ( value_i == 1 )
					{
						/* add to repository */
						printf("Add to repository ---- sec:%s name:%s value:%d\n", section, name_upper, value_i);
						
						if(cur->cfgtype == OPTION_AUTOCONN) {
							/* autoconnnect config : write to "auto" -> "(section name)" sction */
							auto_subsection = find_or_make_subsection(config->repo_auto, subsection_name, CONFIG_TYPE_GROUP);
							add_autoconn_profile_name(auto_subsection, section);
							addconf = find_or_make_subsection(auto_subsection, name_upper, CONFIG_TYPE_BOOL);
							config_setting_set_bool(addconf, 1);
						}else if(cur->cfgtype == OPTION_SYSCONFIG && strlen(section) == 0) {
							/* system config : add repository, "boot" section */
							addconf = find_or_make_subsection(config->repo_boot, name_upper, CONFIG_TYPE_BOOL);
							config_setting_set_bool(addconf, 1);
						}
					}
					break;
					
				/* option is string */ 
				case OPTION_STR:
				
					/* valid if length is larger then 1 */
					if (strlen(value) > 0) {

						// TODO : 内容から「"」を取り除く

						/* add to repository */
						printf("Add to repository ---- sec:%s name:%s value:%s\n", section, name_upper, value);
						
						if(cur->cfgtype == OPTION_AUTOCONN) {
							/* autoconnnect config : write to "auto" -> "(section name)" sction */
							auto_subsection = find_or_make_subsection(config->repo_auto, subsection_name, CONFIG_TYPE_GROUP);
							add_autoconn_profile_name(auto_subsection, section);
							addconf = find_or_make_subsection(auto_subsection, name_upper, CONFIG_TYPE_STRING);
							config_setting_set_string(addconf, value);
						}else if(cur->cfgtype == OPTION_SYSCONFIG && strlen(section) == 0) {
							/* system config : add repository, to "boot" secction */
							addconf = find_or_make_subsection(config->repo_boot, name_upper, CONFIG_TYPE_STRING);
							config_setting_set_string(addconf, value);
						}
					}
					break;
			
				/* option is integer */
				case OPTION_INT:
					/* convert to integer */
					value_i = atoi(value);

					/* TODO : 0がvalue_i に入ってきたら設定を消したい */
					if (value_i != 0)
					{
						/* add to repository */
						printf("Add to repository ---- sec:%s name:%s value:%d\n", section, name_upper, value_i);
						
						if(cur->cfgtype == OPTION_AUTOCONN) {
							/* autoconnnect config : write to "auto" -> "(section name)" sction */
							auto_subsection = find_or_make_subsection(config->repo_auto, subsection_name, CONFIG_TYPE_GROUP);
							add_autoconn_profile_name(auto_subsection, section);
							addconf = find_or_make_subsection(auto_subsection, name_upper, CONFIG_TYPE_INT);
							config_setting_set_int(addconf, value_i);
						}else if(cur->cfgtype == OPTION_SYSCONFIG && strlen(section) == 0) {
							/* system config : add repository, "boot" section */
							addconf = find_or_make_subsection(config->repo_boot, name_upper, CONFIG_TYPE_INT);
							config_setting_set_int(addconf, value_i);
						}
					}
					break;
				
				/* option is file */
				case OPTION_FILE:
					/* TODO */
					break;
				
			}
		}
	}

	/* free heap memory */
	free(name_upper);

	return 1;
}

/* Main Routine */
int main(int argc, char **argv)
{
	int arg;
	char *config_file = NULL;
	char *merged_file = NULL;
	configuration config;
	config_t repo;
	config_setting_t *repo_root, *repo_auto, *repo_boot;

	config.boot_only_config = 1;

	/* parse options */
	while((arg = getopt(argc, argv, "bc:m:?")) != -1)
	{
		switch(arg)
		{
			case 'b':	/* boot only flag */
				config.boot_only_config = 1;
				break;
			case 'c':	/* config file to parse */
				config_file = optarg;
				break;
			case 'm':	/* merged config file */
				merged_file = optarg;
				break;
			case '?':
				printf("Usage: %s -c config_file -m merged_file -b\n", argv[0]);
				printf("   -b : boot only flag\n");
				return 1;
		}
	}
	
	/* check parameter is valid */
	if (config_file == NULL || access(config_file, R_OK) != 0) {
		printf("Error: config file not found or not readable.\n");
		return 1;
	}else{
		printf("config_file: %s\n", config_file);
	}
	
	if (merged_file == NULL) {
		printf("Error: merged file not found.\n");
		return 1;
	}else{
		printf("merged_file: %s\n", merged_file);
	}

	/* init config repository */
	config_init(&repo);
	config.repo = &repo;

	/* すでに存在するmerged_fileであれば、読み込みを試みる */
	if (access(merged_file, F_OK) == 0) {
		if (config_read_file(&repo, merged_file) != CONFIG_TRUE) {
			printf("Error: couldn't open merged file.\n");
			config_destroy(&repo);
			return 1;
		}
	}
	repo_root = config_root_setting(&repo);

	/* auto, boot, cfg セクションを頭出し(なければ作成)しておく */
	repo_auto = config_setting_get_member(repo_root, "auto");
	if (repo_auto == NULL)
		repo_auto = config_setting_add(repo_root, "auto", CONFIG_TYPE_GROUP);
	config.repo_auto = repo_auto;

	repo_boot = config_setting_get_member(repo_root, "boot");
	if (repo_boot == NULL)
		repo_boot = config_setting_add(repo_root, "boot", CONFIG_TYPE_GROUP);
	config.repo_boot = repo_boot;

	/* parse cocon.cnf file */
	// TODO : いったんこのまま [DEFAULT] なしでパースしてどうなるかを見る -> 空文字になる。
	if (ini_parse(config_file, parser_handler, &config) < 0) {
		printf("Error: couldn't load config file.\n");
		config_destroy(&repo);
		return 1;
	}
	
	/* Write config to file */
	if (config_write_file(&repo, merged_file) == CONFIG_FALSE) {
		printf("Error: couldn't write to merged file.\n");
		config_destroy(&repo);
		return 1;
	}
	
	/* cleanup */
	config_destroy(&repo);
	
	return 0;
}

